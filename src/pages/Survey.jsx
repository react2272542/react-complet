import {Link, useParams} from 'react-router-dom'

function Survey() {
    const { questionNumber } = useParams()

    /**
     * Dans Survey/index.jsx, coder un lienprécédentet un liensuivantqui permettent respectivement de passer à la question précédente et à la question suivante.

     Si la question actuelle est à 1, le lien “précédent” reste sur la question 1.

     Si la question est à 10, le lien “suivant” ne s’affiche pas. À la place, il y aura un lien “Résultats” qui redirigera vers la page “Results”.
     */

    const questionNumberInt = parseInt(questionNumber);
    const nextQuestionNumber = questionNumberInt + 1
    const prevQuestionNumber = questionNumberInt === 1 ? 1 : questionNumberInt - 1
    return (
        <div>
        <div>
            <h1>Questionnaire 🧮</h1>
            <h2>Question {questionNumber}</h2>
        </div>
            <div>
                <Link to={`/survey/${prevQuestionNumber}`}>Précédent</Link>
                {questionNumberInt === 10 ? (
                    <Link to="/results">Résultats</Link>
                ) : (
                    <Link to={`/survey/${nextQuestionNumber}`}>Suivant</Link>
                )}
            </div>
        </div>
    )
}

export default Survey